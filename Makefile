.PHONY: all
all: test_fork test_senial

test_fork: test_fork.c
	gcc -o test_fork test_fork.c

test_senial: test_senial.c
	gcc -o test_senial test_senial.c

.PHONY: clean
clean:
	rm test_fork test_senial
