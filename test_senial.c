#include <signal.h>
#include <stdio.h>

void mi_gestionador_senial(int sig)
{
	printf("Hola, se ha presionado CTR-C!!!\n");
}

int main()
{
	signal(SIGINT,mi_gestionador_senial);
	while(1)
	;
}
